import express from 'express';
import { IdentityClient, Queue } from 'wrapper-lib';
import { EventStorage, Logger, RedisEventStorageAdapter } from 'identity-lib';
import { ProtectedModule } from './protected';

const PORT = 3009;
const logger = new Logger();

/*
    This wrapper is using redis as an event storage,
    We should create an instance of redis adapter for
    event storage and inject into the event storage
    object.

    TODO: We are going to use Postgres Adapter
*/
const redisEventStorageAdapter = new RedisEventStorageAdapter({});
const eventStorage = new EventStorage(redisEventStorageAdapter);

/*
    It's recommended for a wrapper to use a queue manager in order
    to process big operations without waiting it to finish.
*/
const queue = new Queue(2, eventStorage);

/**
 * Basic express application
 */
const application = express();

/**
 * Protected module, in this case canvas.
 */
const canvas = new ProtectedModule(application, queue, logger);
canvas.setup();

application.listen(PORT, () => logger.info('Canvas', `Started at ${PORT}`));
