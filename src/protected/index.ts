import {Application} from 'express';
import {
    Logger,
    Source,
} from 'identity-lib';
import {Queue, RefreshResponse, WrapperProtectedAPI} from 'wrapper-lib';
import {People} from './people';
import {Sync} from './sync';
import * as fetch from './fetch';

const base =
    process.env.NODE_ENV === 'production'
        ? 'https://ed.link'
        : 'http://localhost:8080';
const server_base =
    process.env.NODE_ENV === 'production'
        ? 'https://ed.link'
        : 'http://localhost:9000';

/**
 * Protected API is where the Identity will be pushing things.
 * Ex:
 *  Identity Service receives a request from the client to push grades to canvas.
 *  Identity will have to make a request to the canvas wrapper at the mode grades with key create,
 *  the request body is everything needed by this wrapper to perform the call with the provider endpoint.
 *
 */
export class ProtectedModule extends WrapperProtectedAPI {
    constructor(
        application: Application,
        queue: Queue,
        logger: Logger,
        protected readonly sync: Sync = new Sync(),
        protected readonly people: People = new People(),
    ) {
        super(queue, logger, application);
    }

    protected async startSync(source: Source): Promise<any> {
        await fetch.users(source);
        await fetch.sessions(source);
        await fetch.schools(source);
        // await this.fetch_courses(source);
    }

    protected async exchange_token(source, code: any):
        Promise<{ source_access_token: string; source_refresh_token: string; source_access_token_expiration: string }> {
        return Promise.resolve({source_access_token: '', source_access_token_expiration: '', source_refresh_token: ''});
    }

    protected generate_administrator_url(parameters: any): string {
        return '';
    }

    protected generate_login_url({integration, sso}: { integration: any; sso: any }): string {
        return '';
    }

    protected async login(sso, code: any):
        Promise<{ person_source_id: string; person_source_access_token: string; person_source_refresh_token: string;
            person_source_access_token_expiration: string }> {
        return Promise.resolve({
            person_source_access_token: '',
            person_source_access_token_expiration: '',
            person_source_id: '',
            person_source_refresh_token: '',
        });
    }

    protected async refresh(data, sync?: boolean): Promise<RefreshResponse> {
        return Promise.resolve(undefined);
    }

    protected async stopSync(id: string): Promise<any> {
        return Promise.resolve(undefined);
    }
}
