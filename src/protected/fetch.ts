/* tslint:disable:no-shadowed-variable */
import {Model, Person, Source} from 'identity-lib';
import axios, {AxiosRequestConfig} from 'axios';
import * as format from './format';
import querystring from 'querystring';
import OAuth, {RequestOptions} from 'oauth-1.0a';
import {consumer_key, consumer_secret} from '../keys/identity.schoology.js';

const oauth = new OAuth({
    consumer: {
        key: consumer_key,
        secret: consumer_secret,
    },
    signature_method: 'PLAINTEXT',
});

export async function users(source: Source): Promise<Person[]> {
    // Load a list of roles first so we can determine what type of users we are dealing with.
    // As a note, there are some people, who for some reason, we are not able to see (even when accessed directly).
    // We receive a 403 error. It is unknown if this is a bug on Schoology's side.
    // It comes back to bite us later when we ARE able to see the mystery user's enrollments.

    const roles = {};

    const rolesConfig: AxiosRequestConfig = {method: 'get', url: '/roles'};
    const response = await request(source, rolesConfig);

    await new Promise(resolve => setTimeout(resolve, 100));

    if (response.role) {
        for (const role of response.role) {
            roles[role.id] = role;
        }
    }

    return fetch(source, '/users', 'user', user => {
        return format.person(user, roles);
    });
    // IdentityClient.people.hash(..)
}

export async function sessions(source: Source) {
    return fetch(source, '/gradingperiods', 'gradingperiods', format.session);
}

export async function schools(source: Source) {
    const schools = [];

    const schoolsConfig: AxiosRequestConfig = {method: 'get', url: '/schools'};
    const data = await request(source, schoolsConfig);

    await new Promise(resolve => setTimeout(resolve, 100));

    if (data.school) {
        schools.push(...data.school.map(n => format.school(n)));
    }

    for (const school of data.school) {
        const schoolConfig: AxiosRequestConfig = {method: 'get', url: `/schools/${school.id}`};
        const response = await request(source, schoolConfig);

        await new Promise(resolve => setTimeout(resolve, 100));

        if (response.building) {
            schools.push(...response.building.map(bldg => format.school(bldg, school)));
        }
    }
}

export async function fetch<T extends Model>(source: Source, url: string, objectName: string, formatter: (input: any) => Model): Promise<T[]> {
    const items = [];

    const sessionsConfig: AxiosRequestConfig = {method: 'get', url};

    await paginate(source, sessionsConfig, data => {
        if (data[objectName]) {
            items.push(...data[objectName].map(n => formatter(n)));
        }
    });

    return items;
}

export async function request(source: Source, config: AxiosRequestConfig): Promise<any> {
    const keys = {
        key: source.access_token,
        secret: source.refresh_token,
    };

    if (!config.headers) {
        config.headers = {};
    }

    if (config.url.startsWith('/')) {
        config.url = 'https://api.schoology.com/v1' + config.url;
    }

    if (config.params) {
        config.url += `?${querystring.stringify(config.params)}`;
    }

    const options: RequestOptions = config as RequestOptions;
    Object.assign(config.headers, oauth.toHeader(oauth.authorize(options, keys)));

    return axios.request(config).then(res => res.data);
}

/**
 * Processes every page of a paginated Schoology response, and sends responses to
 * the provided callback function.
 * @param source
 * @param config Configuration to apply pagination to.
 * @param callback A function to be called and provided with results for each page.
 * **If this function returns `false`, the pagination will end.**
 * @private
 */
export async function paginate(source: Source, config: AxiosRequestConfig, callback: (result) => any) {
    const params = {
        start: 0,
        limit: 200,
    };

    while (true) {
        config.params.start = params.start;
        config.params.limit = params.limit;

        const response = await request(source, config);
        const shouldEnd: boolean = callback(response) === false;

        if (shouldEnd || response.total <= (params.start + params.limit)) {
            return;
        }

        params.start += params.limit;
        await new Promise(resolve => setTimeout(resolve, 100));
    }
}
