import * as format from './format';
import {Role} from 'identity-lib';

test('format roles & people', () => {
    // tslint:disable-next-line:max-line-length
    const roles = {role: [{id: 754970, title: 'Parent', faculty: 0, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754970'}}, {id: 754969, title: 'Student', faculty: 0, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754969'}}, {id: 754967, title: 'System Admin', faculty: 1, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754967'}}, {id: 754968, title: 'Teacher', faculty: 1, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754968'}}, {id: 754968, title: 'Someone Who Is Faculty', faculty: 1, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754968'}}, {id: 754968, title: 'Someone we don\'t know (custom)', faculty: 0, role_type: 1, links: {self: 'https:\\\\/\\\\/api.schoology.com\\\\/v1\\\\/roles\\\\/754968'}}]};

    expect(format.role(roles.role[0])).toBe(Role.Parent);
    expect(format.role(roles.role[1])).toBe(Role.Student);
    expect(format.role(roles.role[2])).toBe(Role.Administrator);
    expect(format.role(roles.role[3])).toBe(Role.Teacher);
    expect(format.role(roles.role[4])).toBe(Role.Teacher);
    expect(format.role(roles.role[5])).toBe(null);

    // tslint:disable-next-line:max-line-length
    const person = {uid: '89348177', id: 89348177, school_id: 2360553850, synced: 0, school_uid: '', building_id: 2360553850, name_title: '', name_title_show: 0, name_first: 'Dakota', name_first_preferred: '', use_preferred_first_name: '1', name_middle: '', name_middle_show: 0, name_last: 'Gordon', name_display: 'Dakota Gordon', username: '', primary_email: 'dakota@ed.link', picture_url: 'https://asset-cdn.schoology.com/system/files/imagecache/profile_reg/sites/all/themes/schoology_theme/images/user-default.gif', gender: null, position: null, grad_year: '', password: '', role_id: 754967, tz_offset: -6, tz_name: 'America/Chicago', parents: null, child_uids: null, language: '', additional_buildings: ''};

    const result = format.person(person, roles);

    expect(result.source_id).toBe('89348177');
    expect(result.display_name).toBe('Dakota Gordon');
    expect(result.first_name).toBe('Dakota');
    expect(result.last_name).toBe('Gordon');
    expect(result.email).toBe('dakota@ed.link');
    expect(result.time_zone).toBe('America/Chicago');
    expect(result.role).toBe(Role.Administrator);
});
