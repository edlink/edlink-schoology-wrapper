/* tslint:disable:no-shadowed-variable */
import {Enrollment, EnrollmentState, Organization, Person, Role, School, Session, SessionType} from 'identity-lib';

export function person(user: any, roles: any): Person {
    return {
        source_id: user.uid ? user.uid : user.school_id.toString(),
        display_name: user.name_display,
        first_name: user.name_first,
        last_name: user.name_last,
        email: user.primary_email,
        picture_url: user.picture_url,
        locale: user.language, // todo make sure this converts properly
        time_zone: user.tz_name, // todo make sure this converts properly
        role: role(roles.role.find(n => n.id === user.role_id)),
    };
}

const role_by_name = {
    'student': Role.Student,
    'parent': Role.Parent,
    'system admin': Role.Administrator,
    'teacher': Role.Teacher,
};

export function role(role: any): Role {
    const name = role.title?.toLowerCase() ?? '';

    if (role_by_name.hasOwnProperty(name)) {
        return role_by_name[name];
    }

    if (role.faculty) {
        return Role.Teacher;
    }

    return null;
}

export function session(session: any): Session {
    return {
        source_id: session.id.toString(),
        name: session.title,
        type: SessionType.Term,
        start_date: session.start ? new Date(session.start) : null,
        end_date: session.end ? new Date(session.end) : null,
    };
}

export function enrollment(enrollment: any, organization: Organization): Enrollment {
    return {
        source_id: enrollment.id.toString(),
        organization_id: organization.source_id,
        person_id: enrollment.uid.toString(),
        primary: true,
        role: enrollment.admin ? Role.Teacher : Role.Student,
        state: enrollmentState(enrollment.status),
    };
}

const enrollment_state = {
    1: EnrollmentState.Active,
    2: EnrollmentState.Inactive,
    3: EnrollmentState.Pending,
    4: EnrollmentState.Pending,
    5: EnrollmentState.Inactive,
};

export function enrollmentState(status: string): EnrollmentState {
    // https://developer.schoology.com/api-documentation/rest-api-v1/enrollment
    return enrollment_state[status] ?? null;
}

export function school(school: any, parent?: any): School {
    return {
        ancestry: [], // todo ask dakota abt this
        source_id: school.id.toString(),
        parent_source_id: parent ? parent.id.toString() : null,
        name: school.title,
        picture_url: school.picture_url,
    };
}
